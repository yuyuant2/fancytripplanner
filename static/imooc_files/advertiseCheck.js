define(function (require) {
    require("/static/moco/v1.0/dist/js/moco.min.js?t=06111806"), $.get("/common/adver-getadver", function (a) {
        if (a = JSON.parse(a), 0 == a.result) {
            var g = (1e3 * a.data.now_time, a.data.globalRightFloat),
                c = a.data.globalTopBanner,
                h = a.data.readTopBanner,
                v = a.data.articleReply,
                k = a.data.courseIndexBanner,
                w = a.data.coursechapterlistRight,
                R = a.data.courseVideoRight,
                j = a.data.courseVideoWendalistRight,
                A = a.data.articleDetailRight,
                I = a.data.articleIndexRight;
            if (g.length && (S = g[0], moco.initAdv("#globalRightFloat", {
                    image: "//img.mukewang.com/" + S.pic + "-160-160.jpg",
                    link: S.links,
                    advid: S.id,
                    closebtn: !0
                })), c.length && (S = c[0], moco.initAdv("#globalTopBanner", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "slideDown",
                    animateClose: "slideUp",
                    closebtn: !0,
                    nextShowTimeAfterClose: 90
                })), h.length) {
                var S = h[0];
                moco.initAdv("#readTopBanner", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id
                })
            }
            if (v.length) {
                var S = v[0];
                moco.initAdv("#articleReply", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id
                })
            }
            if (A.length) {
                var S = A[0];
                moco.initAdv("#articleDetailRight", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            }
            if (I.length) {
                var S = I[0];
                moco.initAdv("#articleIndexRight", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            }
            if (k.length) {
                var S = k[0];
                moco.initAdv("#courseIndexBanner", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            } else $("#courseIndexBanner").append('<img src="/static/img/course/course-top.png" style="cursor:default">').show();
            if (w.length) {
                var S = w[0];
                moco.initAdv("#coursechapterlistRight", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            }
            if (R.length) {
                var S = R[0];
                moco.initAdv("#courseVideoRight", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            }
            if (j.length) {
                var S = j[0];
                moco.initAdv("#courseVideoWendalistRight", {
                    image: "//img.mukewang.com/" + S.pic + ".jpg",
                    link: S.links,
                    advid: S.id,
                    animateShow: "fadeIn"
                })
            }
        }
    })
});