#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/20 11:12 AM

@author: Tangrizzly
"""

import sqlite3

con = sqlite3.connect("db.sqlite3")
cur = con.cursor()

cur.execute('''DROP TABLE User''')
cur.execute('''CREATE TABLE if NOT EXISTS User(
    userId INTEGER PRIMARY KEY,
    userName TEXT UNIQUE NOT NULL,
    password TEXT,
    email TEXT,
    joinData TEXT,
    avatar TEXT
)''')

cur.execute('''DROP TABLE Login''')
cur.execute('''CREATE TABLE if NOT EXISTS Login(
    userId INTEGER NOT NULL UNIQUE,
    token TEXT NOT NULL,
    FOREIGN KEY(userId) REFERENCES User(userId)
)''')

cur.execute('''DROP TABLE Comments''')
cur.execute('''CREATE TABLE if NOT EXISTS Comments(
    commentId INTEGER PRIMARY KEY,
    contents TEXT NOT NULL,
    date TEXT NOT NULL,
    userId INTEGER NOT NULL,
    reviewId INTEGER NOT NULL,
    FOREIGN KEY(userId) REFERENCES User(userId),
    FOREIGN KEY(reviewId) REFERENCES Reviews(reviewId)
)''')

cur.execute('''DROP TABLE Reviews''')
cur.execute('''CREATE TABLE if NOT EXISTS Reviews(
    reviewId INTEGER PRIMARY KEY,
    review TEXT,
    date TEXT NOT NULL,
    rating INTEGER NOT NULL,
    userId INTEGER NOT NULL,
    attrId INTEGER NOT NULL,
    FOREIGN KEY(userId) REFERENCES User(userId)
)''')


