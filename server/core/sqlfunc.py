#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/28 10:26 PM

@author: Tangrizzly
"""
from django.db import connection


def get_user_count():
    with connection.cursor() as cursor:
        cursor.execute('SELECT COUNT(*) FROM auth_user')
        rows = cursor.fetchall()
    return rows[0][0]


def get_user_info(user_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT id, date_joined, username, email FROM auth_user WHERE id= %s', [user_id])
        rows = cursor.fetchall()
    return [{'id': row[0], 'join_date': row[1], 'username': row[2], 'email': row[3]} for row in rows]


def update_user_info(user_id, username, email):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE auth_user SET username = %s, email = %s '
                       'WHERE id= %s', [username, email, user_id])
        success = cursor.rowcount
    return success


def get_user_reviews(user_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM core_reviews WHERE user_id=%s and deleted=0', [user_id])
        rows = cursor.fetchall()
    return [{'review_id': row[0], 'review': row[1], 'create_date': row[2], 'rating': row[3], 'user_id': row[4], 'attr_id': row[5]} for row in rows]


def get_attr_reviews(attr_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM core_reviews WHERE attr_id=%s and deleted=0', [attr_id])
        rows = cursor.fetchall()
    return [{'review_id': row[0], 'review': row[1], 'create_date': row[2], 'rating': row[3], 'user_id': row[4], 'attr_id': row[5]} for row in rows]


def get_attr_rating(attr_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT avg(rating) FROM core_reviews WHERE attr_id=%s and deleted=0', [attr_id])
        rows = cursor.fetchall()
    return rows[0][0]


def create_a_review(content, rating, user_id, attr_id):
    with connection.cursor() as cursor:
        review_id = cursor.execute('INSERT INTO core_reviews (review, create_date, rating, user_id, attr_id, deleted) '
                       'VALUES(%s, CURRENT_TIMESTAMP, %s, %s, %s, 0)', [content, rating, user_id, attr_id]).lastrowid
    return review_id


def get_a_review(user_id, attr_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM core_reviews WHERE user_id=%s and attr_id = %s and deleted=0', [user_id, attr_id])
        rows = cursor.fetchall()
    return [{'review_id': row[0], 'review': row[1], 'create_date': row[2], 'rating': row[3], 'user_id': row[4], 'attr_id': row[5]} for row in rows]


def update_a_review(user_id, review_id, content, rating):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE core_reviews SET review=%s, rating=%s '
                       'WHERE review_id=%s and user_id=%s and deleted=0', [content, rating, review_id, user_id])
        success = cursor.rowcount
    return success


def delete_a_review(user_id, review_id):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE core_reviews SET deleted=1 '
                       'WHERE review_id=%s and user_id=%s and deleted=0', [review_id, user_id])
        success = cursor.rowcount
    return success


def get_review_comments(review_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM core_comments WHERE review_id=%s and deleted=0', [review_id])
        rows = cursor.fetchall()
    return [{'comment_id': row[0], 'contents': row[1], 'create_date': row[2], 'user_id': row[3], 'review_id': row[4]} for row in rows]


def create_a_comment(content, user_id, review_id):
    with connection.cursor() as cursor:
        review_id = cursor.execute('INSERT INTO core_comments (content, create_date, user_id, review_id, deleted) '
                       'VALUES(%s, CURRENT_TIMESTAMP, %s, %s, 0)', [content, user_id, review_id]).lastrowid
    return review_id


def update_a_comment(user_id, review_id, comment_id, content):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE core_comments SET content=%s'
                       'WHERE review_id=%s and user_id=%s and comment_id=%s and deleted=0', [content, review_id, user_id, comment_id])
        success = cursor.rowcount
    return success


def delete_a_comment(user_id, review_id, comment_id):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE core_comments SET deleted=1 '
                       'WHERE review_id=%s and user_id=%s and comment_id=%s and deleted=0', [review_id, user_id, comment_id])
        success = cursor.rowcount
    return success


def delete_comments(review_id):
    with connection.cursor() as cursor:
        cursor.execute('UPDATE core_comments SET deleted=1 '
                       'WHERE review_id=%s and deleted=0', [review_id])
        success = cursor.rowcount
    return success


def get_user_comments(user_id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT C.comment_id, C.content, C.create_date, R.review_id, R.review, R.create_date, R.rating, R.user_id, R.attr_id FROM core_comments C JOIN core_reviews R ON C.review_id = R.review_id WHERE C.user_id=%s and R.deleted=0 and C.deleted=0', [user_id])
        rows = cursor.fetchall()
    return [{'comment_id': row[0], 'comment': row[1], 'create_date': row[2], 'review_id': row[3], 'review': row[4], 'review_create_date': row[5], 'rating': row[6], 'review_user_id': row[7], 'attr_id': row[8]} for row in rows]


def get_ratings():
    with connection.cursor() as cursor:
        cursor.execute('SELECT user_id, attr_id, rating FROM core_reviews WHERE deleted=0')
        rows = cursor.fetchall()
    return rows


def get_user_with_most_reviews():
    with connection.cursor() as cursor:
        cursor.execute('SELECT distinct A.id, A.username, count(review_id) as count_id '
                       'from auth_user A JOIN core_reviews C on A.id = C.user_id '
                       'GROUP BY A.id '
                       'ORDER BY count_id DESC '
                       'LIMIT 5;')
        rows = cursor.fetchall()
    return [{'user_id': row[0], 'username': row[1], 'count': row[2]} for row in rows]


def get_user_with_highest_rating():
    with connection.cursor() as cursor:
        cursor.execute('select distinct A.id, A.username, avg(rating) as avg_rating '
                       'from auth_user A join core_reviews C on A.id = C.user_id '
                       'group by A.id '
                       'order by avg_rating DESC '
                       'LIMIT 5;')
        rows = cursor.fetchall()
    return [{'user_id': row[0], 'username': row[1], 'rating': row[2]} for row in rows]


def get_review_number(id):
    with connection.cursor() as cursor:
        cursor.execute('SELECT count(review_id) FROM core_reviews WHERE deleted=0 and ')
        rows = cursor.fetchall()
    return rows
