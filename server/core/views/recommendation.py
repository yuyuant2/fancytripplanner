#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/12/2 4:38 PM

@author: Tangrizzly
"""
import numpy as np

from server.core.sqlfunc import get_ratings, get_user_count
from server.core.mongo_models import Attraction_vector, Attraction
from server.core.mongo_models import User_vector
from rest_framework.response import Response

dim = 20
user_num = get_user_count() + 1
attr_num = Attraction.get_attr_count() + 3
epoch = 20
gm = 0.005  # learning rate
lmd = 0.02  # l2


def update_vector(request):
    # initialization
    hu = np.random.rand(user_num, dim)
    ha = np.random.rand(attr_num, dim)
    bu = np.zeros(user_num)
    ba = np.zeros(attr_num)

    # training
    rating = np.asarray(get_ratings())
    avg = np.average(rating[:, 2])

    for e in range(epoch):
        print(e)
        np.random.shuffle(rating)
        for u, a, r in rating:
            r_hat = np.dot(hu[u].T, ha[a]) + bu[u] + ba[a] + avg
            eui = r - r_hat
            bu[u] += gm * (eui - lmd * bu[u])
            ba[a] += gm * (eui - lmd * ba[a])
            ha[a] += gm * (eui * hu[u] - lmd * ha[a])
            hu[u] += gm * (eui * ha[a] - lmd * hu[u])

    # update vectors in database
    User_vector.update_vector(hu.tolist(), bu.tolist())
    Attraction_vector.update_vector(ha.tolist(), ba.tolist(), avg)
    return Response("Success")
