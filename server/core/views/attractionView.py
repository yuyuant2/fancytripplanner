# -*- coding: utf-8 -*-
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import QueryDict
from server.core.mongo_models import Attraction
from server.core.sqlfunc import get_attr_reviews, get_review_comments, get_user_info
from server.core.views.homeView import get_recommendations, get_user_stat
from server.core.views.tokenView import check_token


class AttractionView(APIView):
    def get(self, request):
        context = check_token(request)
        get_recommendations(context)
        get_user_stat(context)
        attr_id = int(request.GET.get('id'))
        context['id'] = attr_id
        context['attraction'] = Attraction.find_attraction_by_id(attr_id)
        if 'hour' in context['attraction']:
            context['attraction']['hour'] = context['attraction']['hour'][5:-6]
        reviews = get_attr_reviews(attr_id)
        for review in reviews:
            review['comments'] = get_review_comments(review['review_id'])
            for comment in review['comments']:
                comment['username'] = get_user_info(comment['user_id'])[0]['username']
                print(comment)
            review['username'] = get_user_info(review['user_id'])[0]['username']
        context['reviews'] = reviews
        context['attraction']['id'] = context['attraction']['_id']
        del context['attraction']['_id']
        return render(request, 'attraction.html', context=context)

    # def get(self, request):
    #     rs = []
    #     if request.GET.get('state'):
    #         rs = Attraction.find_attraction_by_state(request.GET.get('state'))
    #     elif request.GET.get('city'):
    #         rs = Attraction.find_attraction_by_city(request.GET.get('city'))
    #     elif request.GET.get('name'):
    #         rs = Attraction.find_attraction_by_name(request.GET.get('name'))
    #     elif request.GET.get('rating'):
    #         rs = Attraction.find_attraction_by_rating(float(request.GET.get('rating')))
    #     elif request.GET.get('_id'):
    #         rs = Attraction.find_attraction_by_id(int(request.GET.get('_id')))
    #     return Response(rs)

    #  only called when deletd or insert a review by backend, so no auth

    def put(self, request):
        is_insert = False
        put = QueryDict(request.body)

        _id = put.get('_id')
        new_rating = float(put.get('new_rating'))
        if put.get('insert')=='1':
            is_insert = True

        rs = Attraction.update_rating(_id, new_rating, is_insert)
        return Response(rs)

    # only used for populating data, so no auth
    def post(self, request):
        attr = request.POST.dict()
        rs = Attraction.insert_one_attraction(attr)
        inserted_id = rs.inserted_id
        return Response(inserted_id)



