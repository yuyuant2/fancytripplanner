#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/20 12:35 PM

@author: Tangrizzly
"""
from django.shortcuts import render, redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from server.core.sqlfunc import get_user_info, get_user_reviews, get_user_comments, update_a_review, delete_a_review, update_user_info, \
    create_a_review
from server.core.mongo_models import Attraction
from server.core.views.tokenView import check_token


class UserViewProfile(APIView):
    TEMPLATE = 'user_profile.html'

    # get user info
    def get(self, request):
        context = check_token(request)
        profile_user_id = request.GET.get('profile_user_id')
        profile_user = get_user_info(profile_user_id)
        comments = get_user_comments(profile_user_id)
        reviews = get_user_reviews(profile_user_id)
        if not context :
            cur_user_id = -1
            context = {'user_id': cur_user_id, 'reviews': reviews, 'comments': comments, 'profile_user_id': profile_user[0]['id'], 
            'join_date': profile_user[0]['join_date'], 'email': profile_user[0]['email'], 'profile_user_name': profile_user[0]['username'] }

        else:
            cur_user_id = int(context['user_id'])
            cur_user = get_user_info(cur_user_id)
            
            context = {'user_id': cur_user_id, 'username': cur_user[0]['username'], 'reviews': reviews, 'comments': comments,
                    'profile_user_id': profile_user[0]['id'], 'profile_user_name': profile_user[0]['username'],
                    'join_date': profile_user[0]['join_date'], 'email': profile_user[0]['email']}

        return render(request, self.TEMPLATE, context=context)

    # will not show update button
    def post(self, request):
        context = check_token(request)
        user_id = context['user_id']
        username = context['username']
        email = request.POST.get('email')
        url = request.POST.get('url')
        success = update_user_info(user_id, username, email)
        return redirect(url)

class UserViewReviews(APIView):
    TEMPLATE = 'user_reviews.html'

    # get user info
    def get(self, request):
        context = check_token(request)
        if not context:
            cur_user_id = -1
        else:
            cur_user_id = int(context['user_id'])
        
        profile_user_id = request.GET.get('review_user_id')
        tag = request.GET.get('tag')
        profile = get_user_info(profile_user_id)
        comments = get_user_comments(profile_user_id)
        reviews = get_user_reviews(profile_user_id)

        attractions = []
        for review in reviews:
            attractions.append(Attraction.find_attraction_name(review['attr_id']))
        context = {'username': profile[0]['username'], 'reviews': reviews, 'attractions': attractions, 'comments': comments,
                   'user_id': profile[0]['id'], 'profile_user_id': cur_user_id}

        return render(request, self.TEMPLATE, context=context)

    # delete or insert review
    def post(self, request):
        # delete
        if request.POST.get('method')=='delete':
            review_id = request.POST.get('review_id')
            user_id = request.POST.get('user_id')

            success = delete_a_review(user_id, review_id)
            if success == 1:
                return Response({'msg': 'success'})
            else:
                return Response({'msg': 'failed'})
        # insert
        elif request.POST.get('review_id')=='':
            review = request.POST.get('review')
            rating = request.POST.get('rating')
            url = request.POST.get('url')
            user_id = request.POST.get('user_id')
            attraction_id = request.POST.get('attraction_id')
            success = create_a_review(review, rating, user_id, attraction_id)
            if success>0:
                return redirect(url)
        #  update
        else:
            review_id = request.POST.get('review_id')
            review = request.POST.get('review')
            rating = request.POST.get('rating')
            url = request.POST.get('url')
            user_id = request.POST.get('user_id')
            success = update_a_review(user_id, review_id, review, rating)
            if success==1:
                return redirect(url)

    def delete(self, request):
        review_id = request.POST.get('review_id')
        user_id = request.POST.get('user_id')

        success = delete_a_review(user_id, review_id)
        if success == 1:
            return Response({'msg': 'success'})
        
        




# class UserUpdateReview(APIView):
#     TEMPLATE = 'update_review.html'
#
#     # get user info
#     def get(self, request):
#         userId = request.GET.get('user_id')
#         tag = request.GET.get('tag')
#
#         user = get_user_info(userId)
#         comments = get_user_comments(userId)
#         reviews = get_user_reviews(userId)
#         attr_names = []
#         for review in reviews:
#             attr_names.append(Attraction.find_attraction_name(review['attr_id'])['name'])
#         print(reviews)
#         context = {'username': user[0]['username'], 'reviews': reviews, 'attr_names': attr_names, 'comments': comments,
#                    'user_id': user[0]['id'], 'tag': tag}
#         return render(request, self.TEMPLATE, context=context)

