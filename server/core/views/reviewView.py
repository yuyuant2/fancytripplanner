#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/28 11:50 PM

@author: Tangrizzly
"""
import json
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from server.core.mongo_models import Attraction
from server.core.sqlfunc import *
from server.core.views.tokenView import get_token


class userReviewView(APIView):
    def get(self, request):
        user_id = request.GET.get('user_id')
        reviews = get_user_reviews(user_id)
        reviews_comments = []
        for review in reviews:
            review['comments'] = get_review_comments(review['review_id'])
            reviews_comments.append(review)
        return Response(reviews_comments)


class attrReviewView(APIView):
    def get(self, request):
        attr_id = request.GET.get('attr_id')
        reviews = get_attr_reviews(attr_id)
        reviews_comments = []
        for review in reviews:
            review['comments'] = get_review_comments(review['review_id'])
            reviews_comments.append(review)
        return Response(reviews)


class reviewView(APIView):
    # get user info
    def get(self, request):
        user_id = request.GET.get('user_id')
        attr_id = request.GET.get('attr_id')
        review = get_a_review(user_id, attr_id)
        return Response(review)

    def post(self, request):
        token = get_token(request)
        if token is not None:
            user_id = token.user_id
            body = request.POST.dict()
            review_id = create_a_review(body['content'], body['rating'], user_id, body['attr_id'])
            Attraction.update_rating(body['attr_id'])
            return Response(review_id)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    def put(self, request):
        token = get_token(request)
        if token is not None:
            user_id = token.user_id
            body = request.PUT.dict()
            updated_row = update_a_review(user_id, body['review_id'], body['content'], body['rating'])
            Attraction.update_rating(body['attr_id'])
            return Response(updated_row)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    def delete(self, request):
        token = get_token(request)
        if token is not None:
            user_id = token.user_id
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            updated_row = delete_a_review(user_id, body['review_id'])
            if updated_row != 0:
                delete_comments(body['review_id'])
            Attraction.update_rating(body['attr_id'])
            return Response(updated_row)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
