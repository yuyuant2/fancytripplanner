#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/11/23 9:14 PM

@author: Tangrizzly
"""

from django.shortcuts import render
from rest_framework.views import APIView
from server.core.mongo_models import Attraction
from server.core.views.homeView import get_recommendations, get_user_stat
from server.core.views.tokenView import check_token


class searchView(APIView):
    def get(self, request):
        context = check_token(request)
        get_recommendations(context)
        get_user_stat(context)
        wd = request.GET.get('wd')
        tag = request.GET.get('radio')
        context['wd'] = wd
        context['tag'] = tag
        if tag == 'attraction':
            rs = Attraction.mongo_search('name', wd)
        elif tag == 'state':
            rs = Attraction.mongo_search('state', wd)
        else:  # tag == 'city':
            rs = Attraction.mongo_search('city', wd)
        for r in rs:
            r['id'] = r['_id']
        context['results'] = rs
        context['num'] = len(context['results'])
        return render(request, 'result.html', context=context)

