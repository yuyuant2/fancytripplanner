#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/29 1:48 PM

@author: Tangrizzly
"""
import json

from django.shortcuts import render, redirect
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from server.core.views.tokenView import check_token

from server.core.sqlfunc import get_review_comments, create_a_comment, update_a_comment, delete_a_comment, get_user_comments


class userCommentView(APIView):
    def get(self, request):
        user_id = request.GET.get('user_id')
        comments = get_user_comments(user_id)
        return Response(comments)


class commentView(APIView):
    def get(self, request):
        review_id = request.GET.get('review_id')
        review = get_review_comments(review_id)
        return Response(review)

    def post(self, request):
        context = check_token(request)
        if context['user_id'] != '':
            user_id = context['user_id']
            re = request.POST.dict()
            
            # delete a comment
            if request.POST.get('method')=='delete':
                updated_row = delete_a_comment(user_id, re['review_id'], re['comment_id'])
                if updated_row == 1:
                    return Response({'msg': 'success'})
                else:
                    return Response({'msg': 'failed'})
            elif re['comment_id'] !='':
                url = re['url']
                updated_row = update_a_comment(user_id, re['review_id'], re['comment_id'], re['comment'])
           
                if updated_row == 1:
                    return redirect(url)
                
            # insert a comment
            else:
                url = re['url']
                comment_id = create_a_comment(re['comment'], user_id, re['review_id'])
                return redirect(url)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    def put(self, request):
        if 'Authorization' in request.headers:
            token = Token.objects.get(key=request.headers['Authorization'])
            user_id = token.user_id
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            updated_row = update_a_comment(user_id, body['review_id'], body['comment_id'], body['content'])
            return Response(updated_row)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    def delete(self, request):
        if 'Authorization' in request.headers:
            token = Token.objects.get(key=request.headers['Authorization'])
            user_id = token.user_id
            review_id = request.GET.get('review_id')
            comment_id = request.GET.get('comment_id')
            updated_row = delete_a_comment(user_id, review_id, comment_id)
            return Response(updated_row)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class attrCommentView(APIView):
    def get(self, request):
        return 0
