#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/30 11:18 PM

@author: Tangrizzly
"""
from rest_framework import status
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from server.core.views.forms import UserCreationForm


class tokenView(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'user_id': token.user_id, 'token': token.key, 'username': user.username})


class signupView(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            serializer = self.serializer_class(data={'username': request.data['username'],
                                                     'password': request.data['password1']},
                                               context={'request': request})
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            return Response({'user_id': token.user_id, 'token': token.key, 'username': user.username})
        return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)


def check_token(request):
    context = {}
    if 'token' in request.COOKIES and request.COOKIES['token'] != '':
        try:
            token = Token.objects.get(key=request.COOKIES['token'])
            context['username'] = token.user.username
            context['user_id'] = token.user_id
        except Token.DoesNotExist:
            print('Token expired.')
    return context


def get_token(request):
    try:
        token = Token.objects.get(key=request.COOKIES['token'])
        return token
    except:
        return None
