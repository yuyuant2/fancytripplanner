#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/11/23 7:53 PM

@author: Tangrizzly
"""

from django.shortcuts import render
import numpy as np
from server.core.sqlfunc import get_user_with_most_reviews, get_user_with_highest_rating
from server.core.views.tokenView import check_token
from server.core.mongo_models import Attraction, User_vector, Attraction_vector

USERS_WITH_MOST_REVIEWS = get_user_with_most_reviews()
USERS_WITH_HIGHTEST_RATING = get_user_with_highest_rating()
MOST_VOTED_CITIES = Attraction.most_voted_city(5)
MOST_VOTED_STATES = Attraction.most_voted_state(5)
MOST_VOTED_ATTRACTION = Attraction.most_voted_attraction(5)
user_vector = User_vector.get_vector()
attr_vector = Attraction_vector.get_vector()


def home_view(request):
    context = check_token(request)
    get_recommendations(context)
    return render(request, 'home.html', context=context)


def get_user_recommendation(user_id):
    hu, bu = user_vector[0][user_id], user_vector[1][user_id]
    ha, ba, avg = attr_vector
    hu, bu, ha, ba = map(np.asanyarray, (hu, bu, ha, ba))
    scores = np.dot(hu, ha.T) + bu + ba + avg
    max_10 = np.argpartition(scores, -10)[-10:]  # O(n)
    sorted_10 = max_10[np.argsort(scores[max_10])][::-1]  # O(nlogn)
    attrs = []
    for attr in sorted_10:
        attr_json = Attraction.find_attraction_by_id(int(attr))
        attr_json['id'] = attr_json['_id']
        attrs.append(attr_json)
    return attrs


def get_recommendations(context):
    if 'user_id' in context and context['user_id'] < len(user_vector):
        context["attractions"] = get_user_recommendation(context['user_id'])
    else:
        context["attractions"] = MOST_VOTED_ATTRACTION
    context["states"] = MOST_VOTED_STATES
    context["cities"] = MOST_VOTED_CITIES


def get_user_stat(context):
    context['users_with_most_reviews'] = USERS_WITH_MOST_REVIEWS
    context['users_with_highest_ratings'] = USERS_WITH_HIGHTEST_RATING
