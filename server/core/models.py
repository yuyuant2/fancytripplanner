#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/26 5:30 PM

@author: Tangrizzly
"""
from django.contrib.auth.models import User
from django.db import models

'''
class User(models.Model):
    userId = models.AutoField(primary_key=True)
    userName = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    email = models.TextField(blank=True, null=True)
    joinDate = models.DateField()


class Login(models.Model):
    userId = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.TextField()
'''


class Reviews(models.Model):
    review_id = models.IntegerField(primary_key=True)
    review = models.TextField(blank=True, null=True)
    create_date = models.DateTimeField()
    rating = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    attr_id = models.IntegerField()
    deleted = models.BooleanField()


class Comments(models.Model):
    comment_id = models.IntegerField(primary_key=True)
    content = models.TextField()
    create_date = models.DateTimeField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    review = models.ForeignKey(Reviews, on_delete=models.CASCADE)
    deleted = models.BooleanField()
