from django.conf import settings
from pymongo import TEXT
import uuid
import time

from server.core.sqlfunc import get_attr_rating

conn = settings.MONGOCLIENT['attractions_db']


class Attraction(object):

    db = conn['attractions']

    # ToDo count attraction number
    @classmethod
    def get_attr_count(cls):
        pipe = [
            {'$match': {'deleted': 0}},
            {'$count': 'name'}
        ]
        result = cls.db.aggregate(pipe)
        return list(result)[0]['name']

    @classmethod
    def insert_one_attraction(cls, attr):
        attr.update({'_id': cls.db.count(), 'rating': 0, 'rating_sum': 0, 'review_num': 0, 'deleted': 0})
        rs = cls.db.insert_one(attr)
        return rs

    
    @classmethod
    def find_attraction_name(cls, _id):
        return cls.db.find_one({'_id': _id, 'deleted': 0}, {'_id': 1, 'name': 1})

    @classmethod
    def find_attraction_by_name(cls, name):
        return cls.db.find_one({'name': name, 'deleted': 0})

    @classmethod
    def find_attraction_by_id(cls, _id):
        return cls.db.find_one({'_id': _id, 'deleted': 0})

    @classmethod
    def find_attraction_by_city(cls, city):
        result = cls.db.find_one({'city': city, 'deleted': 0})
        if result is None:
            return None
        else:
            return result

    @classmethod
    def find_attraction_by_rating(cls, rating):
        result = cls.db.find_one({'rating': {'$gte': rating}, 'deleted': 0})
        if result is None:
            return None
        else:
            return result

    @classmethod
    def find_attraction_by_state(cls, state):
        result = cls.db.find({'state': state, 'deleted': 0})
        if result is None:
            return None
        else:
            return list(result)

    @classmethod
    def find_cities_by_state(cls, state):
        result = cls.db.find({'state': state, 'deleted': 0}, {'city': 1, '_id': 0})
        if result is None:
            return None
        else:
            return list(result)

    @classmethod
    def update_rating(cls, _id):
        cls.db.update_one({'_id': _id}, {'$set': {'rating': get_attr_rating(_id)}})

    @classmethod
    def most_voted_city(cls, k):
        pipe = [
            {'$match': {'deleted': 0}},
            {'$group': {'_id': '$city', 'avg_ratings': { '$avg': "$rating"}, 'attr_cnt': {'$sum': 1}}},
            {'$match': {'attr_cnt': {'$gt': 3}}},
            {'$project':{'_id': 0, 'city': "$_id", 'avg_ratings': 1, 'review_num': 1}},
            {'$sort': {'review_num': 1, 'avg_ratings': -1}},
            {'$limit': k}
        ]
        result = cls.db.aggregate(pipe)
        if result is None:
            return None
        else:
            result = list(result)
            for r in list(result):
                r['avg_ratings'] = round(r['avg_ratings'], 1)
            # print(result)
            return result

    @classmethod
    def most_voted_state(cls, k):
        #review_num
        pipe = [
            {'$match': {'deleted': 0, 'review_num': {'$gt': 2}}},
            {'$group': {'_id': '$state', 'avg_ratings': { '$avg': "$rating"}, 'attr_cnt': {'$sum': 1}}},
            {'$match': {'attr_cnt': {'$gt': 3}}},
            {'$project':{'_id': 0, 'state': "$_id", 'avg_ratings': 1}},
            {'$sort': {'attr_cnt': 1, 'review_num': 1, 'avg_ratings': -1}},
            {'$limit': k}
        ]
        result = cls.db.aggregate(pipe)
        if result is None:
            return None
        else:
            result = list(result)
            for r in list(result):
                r['avg_ratings'] = round(r['avg_ratings'], 1)
            return result

    @classmethod
    def most_voted_attraction(cls, k):
        pipe = [

            {'$match': {'deleted': 0, 'review_num': {'$gt': 2}}},
            {'$project':{'id': '$_id', 'name': "$name", 'rating': 1, 'state': 1, 'city': 1}},
            {'$sort': {'review_num': 1}},
            {'$limit': k}
        ]
        result = cls.db.aggregate(pipe)
        if result is None:
            return None
        else:
            return list(result)

    #  get index info of current mongodb database
    @classmethod
    def index_info(cls):
        return cls.db.index_information()

    # if current index is not same as our search tag ('state', 'city'...), reset index
    # exact phase on state and city search, logical OR on attraction search
    # return result in DESCENDING rating order
    @classmethod
    def mongo_search(cls, index_str, search_str):

        index_info = cls.db.index_information()
        if len(index_info) < 2:
            Attraction.reset_index(index_str)
        else:
            cur_index = list(index_info.keys())[1]
            if cur_index != index_str:
                Attraction.reset_index(index_str)
        
        if index_str=='state' or index_str=='city':
            search_str = ''.join(['"', search_str, '"'])

        result = cls.db.find({"$text": {"$search": search_str}, 'deleted': 0}).sort("rating",-1)  
        return list(result)

    # reset index
    @classmethod
    def reset_index(cls, index_str):
        cls.db.drop_indexes()
        if len(cls.db.index_information()) == 1:
            cls.db.create_index([(index_str, TEXT)], default_language='english')

        return cls.db.index_information()


class Attraction_vector(object):

    db = conn['attraction_vector']

    # only one doc in the entire collection
    @classmethod
    def get_vector(cls):
        json = list(cls.db.find({}, {'_id': 0}))[0]
        return json['ha'], json['ba'], json['avg']
        
    # update entire collection
    @classmethod
    def update_vector(cls, ha, ba, avg):
        # empty collection first
        cls.db.delete_many({})
        vector = {'ha': ha, 'ba': ba, 'avg': avg}
        return cls.db.insert_one(vector)


class User_vector(object):

    db = conn['user_vector']

    # only one doc in the entire collection
    @classmethod
    def get_vector(cls):
        json = list(cls.db.find({}, {'_id': 0}))[0]
        return json['hu'], json['bu']

    # update entire collection
    @classmethod
    def update_vector(cls, hu, bu):
        # empty collection first
        cls.db.delete_many({})
        vector = {'hu': hu, 'bu': bu}
        return cls.db.insert_one(vector)



