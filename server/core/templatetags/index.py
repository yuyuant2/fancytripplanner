from django import template
import json
register = template.Library()

@register.filter
def index_name(indexable, i):
    return indexable[i]['name']

@register.filter
def index_id(indexable, i):
    return indexable[i]['_id']

@register.filter
def name_to_list(indexable, i):
    return [indexable[i]['name']]

@register.filter
def name_to_list_1(name):
    return [name]

@register.filter
def get_key(map, key):
    return map[key]

@register.filter
def get_info(map):
    review = {
        'review_id': map['review_id'],
        'review': map['review'],
        'rating': map['rating'],
    }
    return json.dumps(review)

@register.filter
def get_comment_info(map):
    review = {
        'comment_id': map['comment_id'],
        'comment': map['contents'],
        'review_id': map['review_id']
    }
    return json.dumps(review)

@register.filter
def user_to_json(map):
    return json.dumps(map)
