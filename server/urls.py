"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from server.core.views.commentView import commentView, userCommentView, attrCommentView
from server.core.views.homeView import home_view
from server.core.views.recommendation import update_vector
from server.core.views.reviewView import userReviewView, reviewView, attrReviewView
from server.core.views.searchView import searchView
from server.core.views.tokenView import tokenView, signupView
from server.core.views.userView import UserViewProfile, UserViewReviews
from server.core.views.attractionView import AttractionView

'''
References:
https://docs.djangoproject.com/en/2.2/topics/http/urls/
https://docs.djangoproject.com/en/2.2/topics/class-based-views/intro/
'''

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view, name='home'),

    path('signup/', signupView.as_view(), name='signup'),  # registration/signup.html
    path('login/', tokenView.as_view(), name='login'),  # use 'get token' to replace login, do not have a html
    path('user/profile/', UserViewProfile.as_view(), name='user_profile'),  # \w+?
    path('user/reviews/', UserViewReviews.as_view(), name='user_reviews'),  # \w+?
    # path('user/reviews/update-review/', UserUpdateReview.as_view(), name='update_review'),  # \w+?

    path('reviewlist/user/', userReviewView.as_view()),
    path('reviewlist/attr/', attrReviewView.as_view()),
    path('review/', reviewView.as_view()),

    path('commentlist/user/', userCommentView.as_view()),
    path('commentlist/attr/', attrCommentView.as_view()),
    path('comment/', commentView.as_view()),

    path('attraction/', AttractionView.as_view()),

    path('s/', searchView.as_view(), name='s'),

    path('reco/', update_vector),
]

# handler404 = 'blog.views.page_not_found_view'
# handler500 = 'blog.views.server_error_view'
# handle403 = 'blog.views.permission_denied_view'
