#!/usr/bin/env python36
# -*- coding: utf-8 -*-
"""
Created on 2019/10/30 8:18 PM

@author: Tangrizzly
"""
import json
import re
import requests

upload_attr = True

states = {'AL': 'ALABAMA', 'AK': 'ALASKA', 'AS': 'AMERICAN SAMOA', 'AZ': 'ARIZONA', 'AR': 'ARKANSAS', 'CA': 'CALIFORNIA', 'CO': 'COLORADO', 'CT': 'CONNECTICUT', 'DE': 'DELAWARE', 'DC': 'DISTRICT OF COLUMBIA', 'FM': 'FEDERATED STATES OF MICRONESIA', 'FL': 'FLORIDA', 'GA': 'GEORGIA', 'GU': 'GUAM GU', 'HI': 'HAWAII', 'ID': 'IDAHO', 'IL': 'ILLINOIS', 'IN': 'INDIANA', 'IA': 'IOWA', 'KS': 'KANSAS', 'KY': 'KENTUCKY', 'LA': 'LOUISIANA', 'ME': 'MAINE', 'MH': 'MARSHALL ISLANDS', 'MD': 'MARYLAND', 'MA': 'MASSACHUSETTS', 'MI': 'MICHIGAN', 'MN': 'MINNESOTA', 'MS': 'MISSISSIPPI', 'MO': 'MISSOURI', 'MT': 'MONTANA', 'NE': 'NEBRASKA', 'NV': 'NEVADA', 'NH': 'NEW HAMPSHIRE', 'NJ': 'NEW JERSEY', 'NM': 'NEW MEXICO', 'NY': 'NEW YORK', 'NC': 'NORTH CAROLINA', 'ND': 'NORTH DAKOTA', 'MP': 'NORTHERN MARIANA ISLANDS', 'OH': 'OHIO', 'OK': 'OKLAHOMA', 'OR': 'OREGON', 'PW': 'PALAU', 'PA': 'PENNSYLVANIA', 'PR': 'PUERTO RICO', 'RI': 'RHODE ISLAND', 'SC': 'SOUTH CAROLINA', 'SD': 'SOUTH DAKOTA', 'TN': 'TENNESSEE', 'TX': 'TEXAS', 'UT': 'UTAH', 'VT': 'VERMONT', 'VI': 'VIRGIN ISLANDS', 'VA': 'VIRGINIA', 'WA': 'WASHINGTON', 'WV': 'WEST VIRGINIA', 'WI': 'WISCONSIN', 'WY': 'WYOMING', 'AE': 'ARMED FORCES AFRICA \ CANADA \ EUROPE \ MIDDLE EAST', 'AA': 'ARMED FORCES AMERICA (EXCEPT CANADA)', 'AP': 'ARMED FORCES PACIFIC'}

with open('attraction_review_user.json', 'r') as f:
    data = f.readlines()

with open('about_hours_addr.json', 'r') as f:
    attr_data = f.readlines()

attr = dict()
user = dict()

for plain_line in attr_data:
    line = json.loads(plain_line)
    for key, value in line.items():
        attr[key] = [len(attr)] + value
        if upload_attr:
            addr = value[2].split(',')
            while len(addr) > 0 and addr[-1] == 'None':
                addr = addr[:-1]
            if len(addr) < 2:
                continue
            body = {'name': key}
            if value[0] is not None:
                body['intro'] = value[0]
            if value[1] is not None:
                body['hour'] = value[1]
            if len(addr) == 2:
                body['city'] = addr[0]
                addr[-1] = addr[-1].strip()
                body['state'] = states[addr[-1].split(' ')[0]]
            else:
                body['city'] = addr[-2]
                addr[-1] = addr[-1].strip()
                body['state'] = states[addr[-1].split(' ')[0]]
                body['address'] = ', '.join(addr)
                if key == 'Chicago Skyline':
                    temp = 1
            r = requests.post(url="http://localhost:8000/attraction/", data=body)
            if r.status_code != 200:
                print('insert attraction failed.')

for plain_line in data:
    line = json.loads(plain_line)
    for key, reviews in line.items():
        for review in reviews:
            username = re.sub('[^a-zA-Z0-9]+', '', review[2][1])

            if key not in attr:
                continue
            if username not in user:
                user[username] = len(user) + 1
                URL = "http://localhost:8000/signup/"
                body = {'username': username, 'email': 'fake@email.com', 'password1': review[2][0][:16], 'password2': review[2][0][:16]}
                r = requests.post(url=URL, data=body)
                if r.status_code != 200:
                    print('insert user failed.')

            URL = "http://localhost:8000/review/"
            body = {'user_id': user[username], 'content': review[0], 'rating': review[1], 'attr_id': attr[key][0]}
            r = requests.post(url=URL, json=body)
            if r.status_code != 200:
                print('insert review failed.')
