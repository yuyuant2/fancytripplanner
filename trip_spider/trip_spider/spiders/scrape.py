import scrapy
from scrapy.selector import Selector
import json

#'https://www.tripadvisor.com/Attractions-g43323-Activities-a_allAttractions.true-Minneapolis_Minnesota.html'

class QuotesSpider(scrapy.Spider):
    name = "state-attraction"

    def start_requests(self):
        fir_part = 'https://www.tripadvisor.com/Attractions-g289'
        sec_part = '-Illinois-Vacations.html'
        urls = []
        for i in range(22,70):
            urls.append(fir_part + str(i) + sec_part)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        f = open("state_attraction.txt","a+")
        data = {}
        state_num = response.url.split("/")[-1].split('-')[1][4:]
        state_name = response.url.split("/")[-1].split('-')[3].split('.')[0]
        attractions = []
        for i in range(1,50):
            element = Selector(response=response).xpath('//div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[' + str(i) + ']/div/div/div/div[1]/div/div[1]/div[3]/a/text()').get()
            attractions.append(element)
        data[state_name] = attractions
        json.dump(data, f)
        f.write('\n')
        f.close()

#https://www.tripadvisor.com/Tourism-g28934-Illinois-Vacations.html
#https://www.tripadvisor.com/Attractions-g28944-Activities-Minnesota.html
        
class StateUrlSpider(scrapy.Spider):
    name = "review-pre"

    def start_requests(self):
        fir_part = 'https://www.tripadvisor.com/Attractions-g289'
        sec_part = '-Illinois-Vacations.html'
        urls = []
        for i in range(22, 70):
        #i = 50
            urls.append(fir_part + str(i) + sec_part)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

#/html/body/div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[1]/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a
    def parse(self, response):
        f = open("map_attraction_url.json", "a+")
        data = {}
        state_num = response.url.split("/")[-1].split('-')[1][4:]
        state_name = response.url.split("/")[-1].split('-')[3].split('.')[0]
        attractions = []
        for i in range(1, 50):
            element = {}
            k = Selector(response=response).xpath(
                '//div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[' + str(i) + ']/div/div/div/div[1]/div/div[1]/div[3]/a/text()').get()
            v = Selector(response=response).xpath(
                '//div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[' + str(i) + ']/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a/@href').get()
            element[k] = v
            attractions.append(element)
        data[state_name] = attractions
        json.dump(data, f)
        f.write('\n')
        f.close()


class TripAdvisorReview(scrapy.Spider):
    name = "tripadvisor"
    start_urls = []
#https://www.tripadvisor.com/ShowUserReviews-g43044-d1673922-r696285160-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html#REVIEWS
    
    def start_requests(self):
        with open('attration_review_user.json', mode='w', encoding='utf-8') as f:
            json.dump({}, f)
        fir_part = 'https://www.tripadvisor.com/Attractions-g289'
        sec_part = '-Illinois-Vacations.html'
        urls = []
        for i in range(22, 70):
            urls.append(fir_part + str(i) + sec_part)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_url)

#/html/body/div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[1]/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a
    def parse_url(self, response):
        for i in range(1, 10):
            v = Selector(response=response).xpath(
                '//div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[' + str(i) + ']/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a/@href').get()
            #print(v)
            self.start_urls.append('https://www.tripadvisor.com' + v)
            next_page = 'https://www.tripadvisor.com' + v
            for j in range(1, 2):
                firstpart = next_page.split('-')[:3]
                secondpart = next_page.split('-')[4:]
                firstpart.append('or' + str(j) + '0')
                next_page = '-'.join(firstpart + secondpart)
                print(next_page)
                self.start_urls.append(next_page)


    # with page number https://www.tripadvisor.com/Attraction_Review-g43044-d1673922-Reviews-or30-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html
    # https: // www.tripadvisor.com/Attraction_Review-g43044-d1673922-Reviews-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html

        
        urls = self.start_urls
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        first_ten_revid = Selector(response=response).xpath(
            '//*[@class="review-container"]/@data-reviewid').getall()
        # each page has 10 review ids
        urls = []
        # response url https://www.tripadvisor.com/Attraction_Review-g43044-d1673922-Reviews-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html
        for i in range(len(first_ten_revid)):
            idx = first_ten_revid[i]
            old_url = str(response.url)
            #print(old_url.split('/'))
            new_url_start = 'https://www.tripadvisor.com/ShowUserReviews-'
            g_field = old_url.split('/')[3].split('-')[1]
            d_field = old_url.split('/')[3].split('-')[2]
            last_list = old_url.split('/')[3].split('-')[4:] 
            end = '-'.join(last_list)+ "#REVIEW"
            new_idx = 'r' + str(idx)
            url = new_url_start+g_field+'-'+d_field+'-'+new_idx+'-'+end
            urls.append(url)
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_reviews)
    
    def parse_reviews(self,response):
        review = response.xpath(
            '//div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div[1]/div/div/div/div[2]/div[3]/div/p/span/text()').get()
        rate = response.xpath(
            '//div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div[1]/div/div/div/div[2]/span[1]/@class').get()
        rate = rate[-2]
        attraction = response.xpath('//*[@id="heading"]/text()').get()
        userId = response.xpath(
            '//div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div[1]/div/div/div/div[1]/div/div/div[1]/@id').get()
        userName = response.xpath(
            '//div[2]/div[2]/div[1]/div[1]/div/div[2]/div/div/div/div[1]/div/div/div/div[1]/div/div/div[1]/div[2]/div[1]/text()').get()

        with open('attration_review_user.json') as f:
            data = json.load(f)
        if attraction not in data:
            data[attraction] = [(review,rate,(userId,userName))]
        else:
            data[attraction].append((review, rate, (userId, userName)))
    
        with open('attration_review_user.json', 'w') as f:
            json.dump(data, f)


class AttractionAbout(scrapy.Spider):
    name = "attr"
    start_urls = []

    # https://www.tripadvisor.com/ShowUserReviews-g43044-d1673922-r696285160-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html#REVIEWS

    def start_requests(self):
        with open('tmp.json', mode='w', encoding='utf-8') as f:
            json.dump({}, f)
        fir_part = 'https://www.tripadvisor.com/Attractions-g289'
        sec_part = '-Illinois-Vacations.html'
        urls = []
        for i in range(22, 70):
            urls.append(fir_part + str(i) + sec_part)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_url)

    # /html/body/div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[1]/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a
    def parse_url(self, response):
        for i in range(1, 30):
            v = Selector(response=response).xpath(
                '//div[4]/div[2]/div/div[1]/div/div[2]/div[10]/div[' + str(
                    i) + ']/div/div/div/div[1]/div/div[1]/div[4]/div[2]/div/span[2]/a/@href').get()
            # print(v)
            self.start_urls.append('https://www.tripadvisor.com' + v)
        # with page number https://www.tripadvisor.com/Attraction_Review-g43044-d1673922-Reviews-or30-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html
        # https: // www.tripadvisor.com/Attraction_Review-g43044-d1673922-Reviews-Chilly_Dogs_Sled_Dog_Trips-Ely_Minnesota.html
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.parse_reviews)

    def parse_reviews(self, response):
        attraction = response.xpath('//div[1]/div/div[1]/div[5]/div/div/div/div[2]/ul/li[5]/text()').get()
        about = response.xpath('//div[2]/div[2]/div[2]/div[2]/div/div/div[2]/div/div/div/div/div[2]/span[1]/text()').get()
        hours = response.xpath('//div[2]/div[2]/div[2]/div[2]/div/div/div[2]/div/div/div/div/div[4]/div/div/div/div/div[2]').get()

        street_addr = response.xpath('//div[2]/div[1]/div/div[3]/div/div[2]/div/div[1]/div/div/span[2]/span[1]/text()').get()
        extended = response.xpath('//div[2]/div[1]/div/div[3]/div/div[2]/div/div[1]/div/div/span[2]/span[2]/text()').get()
        local = response.xpath('//div[2]/div[1]/div/div[3]/div/div[2]/div/div[1]/div/div/span[2]/span[3]/text()').get()
        addr = str(street_addr)+','+str(extended)+','+str(local)

        with open('tmp.json') as f:
            data = json.load(f)

        data[attraction] = (about, hours, addr)

        with open('tmp.json', 'w') as f:
            json.dump(data, f)


