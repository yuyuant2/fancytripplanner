# Fancy Trip Planner

This repo provides a search engine for attractions integrated with a personalized recommendation system to help users generate a more considerate travel plan.

## Setup
1. set up python environment from requirements.txt
    ```
    pip install -r requirements.txt
    ```
2. start django server
    ```
    python3.6 manage.py runserver 127.0.0.1:8000
    ```

## Database

The attraction information (reviews, rating, location, contact info, open hours etc), and user information (reviews written by this user, average ratings this user gives), are crawled from Trip advisor (https://www.tripadvisor.com/). In total, we use 947 attractions,and 6040 reviews.

### db.sqlite3

A MySQL database is used to store user information, reviews, and comments. There are 3 tables in practice:
- User profile table: userid, username, password, email, and join_time. 
- Review table: review, userid, reviewid, create_time, rating. 
- Comment table: commentid, comment, create_date, reviewid, and userid.

Configuring `db.sqlite3`:
```
python manage.py makemigrations
python manage.py migrate --run-syncdb
```

### MongDB

A NoSQL database is used for attraction information. Each attraction profile includes attraction name, introduction, contact, open hours, average rating, and other info that differ from location to location. The NoSQL database also stores users' scores on attractions used for personalized recommendation.
